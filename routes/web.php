<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'user'], function () use ($router) {
    $router->post('login', 'AuthController@login');
});

$router->group(['prefix' => 'ref'], function () use ($router) {
    $router->get('diagnosa', 'ReferenceController@refDiagnosa');
    $router->get('therapy', 'ReferenceController@refTherapy');
    $router->get('obat', 'ReferenceController@refObat');
    $router->post('refregkehamilan', 'ReferenceController@refRegKehamilan');
    $router->get('refrekkehamilan', 'ReferenceController@refRekKehamilan');
});

$router->group(['prefix' => 'super-admin'], function () use ($router) {
    $router->get('jabatan', 'SuperAdminController@ListJabatan');
    $router->get('pegawai', 'SuperAdminController@ViewPegawai');
    $router->post('add-pegawai', 'SuperAdminController@CreatePegawai');
    $router->post('delete-pegawai', 'SuperAdminController@DeletePegawai');
    // $router->post('create-user', 'SuperAdminController@CreateUser');
    // $router->delete('delete-user', 'SuperAdminController@DeleteUser'); //belum jalan
});

$router->group(['prefix' => 'administrasi'], function () use ($router) {
    $router->get('count-antrian', 'AdministrasiController@CountAntrian');
    $router->get('no_reg', 'AdministrasiController@GenerateNoReg');
    $router->get('no_antrian', 'AdministrasiController@GenerateNoAntrian');
    $router->get('cek-reg', 'AdministrasiController@CekNoReg');
    $router->post('doktor/add-pasien/baru', 'AdministrasiController@CreatePasienDoktorBaru');
    $router->post('bidan/add-pasien/baru', 'AdministrasiController@CreatePasienBidanBaru');
    $router->get('antrian', 'AdministrasiController@ListAntrian');
    $router->post('exist-pasien', 'AdministrasiController@CreatePasienExist');
    // $router->post('create-pasien', 'AdministrasiController@CreatePasien');
});

$router->group(['prefix' => 'doktor'], function () use ($router) {
    $router->get('count-antrian', 'DoktorController@CountAntrian');
    $router->get('pasien', 'DoktorController@ListPasien');
});

$router->group(['prefix' => 'bidan'], function () use ($router) {
    $router->get('count-antrian', 'BidanController@CountAntrian');
    $router->get('pasien', 'BidanController@ListPasien');
    $router->get('/pasien/penanganan', 'BidanController@DetailAntrianImunisasi');
    $router->get('/pasien/kms', 'BidanController@ListKMS');
    $router->get('/pasien/imunisasi', 'BidanController@ListImunisasi');
    $router->post('/pasien/add-kms', 'BidanController@CreateKMS');
    $router->put('/pasien/penanganan-selesai/{no_antrian}', 'BidanController@PenangananSelesai');
    $router->put('/pasien/update-imunisasi', 'BidanController@UpdateImunisasi');
    $router->post('/pasien/add-kehamilan', 'BidanController@CreateKehamilan');
    $router->get('/pasien/kehamilan', 'BidanController@DetailKehamilan');

    $router->post('/pasien/rekamkehamilan', 'BidanController@rekKehamilan');
});
