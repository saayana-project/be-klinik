<?php

namespace App\Http\Controllers;

use App\Models\Jabatan;
use App\Models\User;
use App\Models\Pegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;

use Laravel\Lumen\Routing\Controller as BaseController;

class SuperAdminController extends BaseController
{
    public function ListJabatan(Request $request)
    {
        $jabatan = Jabatan::all();
        if ($jabatan) {
            return response()->json(
                [
                    'message' => 'Sukses! Ambil Data.',
                    'data' => $jabatan
                ],
                200
            );
        }
    }

    public function ViewPegawai(Request $request)
    {
        $pegawai = Pegawai::with('jabatan', 'user')->get();

        // $pegawai->transform(function ($item, $key) {
        //     $item->nama_jabatan = $item->jabatan ? $item->jabatan->nama_jabatan : null;
        //     return $item;
        // });

        if ($pegawai) {
            return response()->json(
                [
                    'message' => 'Sukses! Ambil Data.',
                    'data' => $pegawai
                ],
                200
            );
        }
    }

    // public function CreateUser(Request $request)
    // {
    //     $this->validate($request, [
    //         'name' => 'required',
    //         'email' => 'required|email',
    //         'password' => 'required',
    //         'level' => 'required'
    //     ]);

    //     $user = new User;
    //     $user->name = $request->name;
    //     $user->email = $request->email;
    //     $user->password = Hash::make($request->password);
    //     $user->level = $request->level;

    //     if ($user->name == null || $user->email == null || $user->password == null) {
    //         return response()->json(['message' => 'Tambah user gagal!'], 401);
    //     } else {
    //         try {
    //             $user->save();
    //         } catch (QueryException $exception) {
    //             if ($exception->errorInfo[1] === 1062) {
    //                 return response()->json(['message' => 'Email user sudah terdaftar!'], 401);
    //             }
    //             return response()->json(['message' => 'Terjadi kesalahan saat menyimpan data!'], 401);
    //         }
    //         $user->save();
    //         return response()->json(['message' => 'Berhasil menambah user!'], 200);
    //     }
    // }

    // public function DeleteUser(Request $id)
    // {
    //     $data = User::find($id);

    //     if ($data) {
    //         $data->delete();
    //         return response()->json(['message' => 'Data berhasil dihapus'], 200);
    //     } else {
    //         return response()->json(['message' => 'Data tidak ditemukan'], 404);
    //     }
    // }

    public function CreatePegawai(Request $request)
    {
        $this->validate($request, [
            'alamat' => 'required',
            'email' => 'required|email',
            'kode_jabatan' => 'required',
            'nama_pegawai' => 'required',
            'password' => 'required',
            'tgl_lahir' => 'required',
        ]);

        $pegawai = new Pegawai;
        $pegawai->nama_pegawai = $request->nama_pegawai;
        $pegawai->kode_jabatan = $request->kode_jabatan;
        $pegawai->tgl_lahir = $request->tgl_lahir;
        $pegawai->email = $request->email;
        $pegawai->alamat = $request->alamat;

        $user = new User;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->kode_jabatan = $request->kode_jabatan;

        if ($pegawai->email == null) {
            return response()->json(['message' => 'Tambah pegawai gagal!'], 401);
        } else {
            try {
                $pegawai->save();
                $user->save();
            } catch (QueryException $exception) {
                if ($exception->errorInfo[1] === 1062) {
                    return response()->json(['message' => 'Email pegawai sudah terdaftar!'], 401);
                }
                return response()->json(['message' => 'Terjadi kesalahan saat menyimpan data!'], 401);
            }
            $pegawai->save();
            $user->save();
            return response()->json(['message' => 'Berhasil menambah pegawai!'], 200);
        }
    }

    public function DeletePegawai(Request $id)
    {
        $user = User::where('email', $id->email)->first();
        $pegawai = Pegawai::where('email', $id->email)->first();

        if ($user && $pegawai) {
            $user->delete();
            $pegawai->delete();
            return response()->json(['message' => 'Data berhasil dihapus'], 200);
        } else {
            return response()->json(['message' => 'Data tidak ditemukan'], 404);
        }
    }
}
