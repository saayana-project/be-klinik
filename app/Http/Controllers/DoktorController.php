<?php

namespace App\Http\Controllers;

use App\Models\Antrian;
use App\Models\History;
use App\Models\Pasien;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;


use Laravel\Lumen\Routing\Controller as BaseController;

class DoktorController extends BaseController
{
    public function CountAntrian(Request $request)
    {
        $today = Carbon::today();

        $count = Antrian::whereDate('created_at', $today)
            ->where('status', '1')
            ->count();

        return response()->json([
            'message' => 'Sukses ambil data.',
            'count' => $count
        ]);
    }

    public function ListPasien(Request $request)
    {
        $tindakan = $request->query('tindakan');
        $date = $request->query('date');

        $query = Antrian::join('pasien', 'antrian.no_reg', '=', 'pasien.no_reg')
            ->join('rekam_medis', 'pasien.no_reg', '=', 'rekam_medis.no_reg')
            ->select('pasien.*', 'antrian.no_antrian', 'antrian.created_at', 'antrian.tindakan', 'antrian.status', 'antrian.soft_delete', 'rekam_medis.tanggal')
            ->where('antrian.status', 1)
            ->orderBy('antrian.created_at', 'desc');

        if ($tindakan !== null) {
            $query->where('antrian.tindakan', $tindakan);
        }

        if ($date !== null) {
            $query->whereDate('antrian.created_at', '=', $date);
        }

        $rawData = $query->get();

        $data = [];
        foreach ($rawData as $item) {
            if (!isset($data[$item->no_reg])) {
                $data[$item->no_reg] = [
                    'no_reg' => $item->no_reg,
                    'nama' => $item->nama,
                    'nik' => $item->nik,
                    'tgl_lahir' => $item->tgl_lahir,
                    'umur_pasien' => $item->umur_pasien,
                    'nama_pj' => $item->nama_pj,
                    'status_pj' => $item->status_pj,
                    'no_hp' => $item->no_hp,
                    'pekerjaan' => $item->pekerjaan,
                    'alamat' => $item->alamat,
                    'golongan_darah' => $item->golongan_darah,
                    'tinggi_badan' => $item->tinggi_badan,
                    'berat_badan' => $item->berat_badan,
                    'antrian' => [
                        'no_antrian' => $item->no_antrian,
                        'tindakan' => $item->tindakan,
                        'status' => $item->status,
                        'soft_delete' => $item->soft_delete,
                        'created_at' => $item->created_at
                    ],
                ];
            }

            $riwayatItem = [];
            $riwayatItem['tanggal'] = $item->tanggal;
        }

        $data = array_values($data);

        return response()->json([
            'message' => 'Sukses ambil data.',
            'data' => $data
        ], 200);
    }

    // public function ListPasien(Request $request)
    // {
    //     $tindakan = $request->query('tindakan');
    //     $date = $request->query('date');

    //     $query = Antrian::join('pasien', 'antrian.no_reg', '=', 'pasien.no_reg')
    //         ->join('rekam_bidan', 'pasien.no_reg', '=', 'rekam_bidan.no_reg')
    //         ->select('pasien.*', 'antrian.no_antrian', 'antrian.created_at', 'antrian.tindakan', 'antrian.status', 'antrian.soft_delete', 'rekam_bidan.tanggal')
    //         ->where('antrian.status', 2)
    //         ->orderBy('antrian.created_at', 'desc');

    //     if ($tindakan !== null) {
    //         $query->where('antrian.tindakan', $tindakan);
    //     }

    //     if ($date !== null) {
    //         $query->whereDate('antrian.created_at', '=', $date);
    //     }

    //     $rawData = $query->get();

    //     $data = [];
    //     foreach ($rawData as $item) {
    //         if (!isset($data[$item->no_reg])) {
    //             $data[$item->no_reg] = [
    //                 'no_reg' => $item->no_reg,
    //                 'nama' => $item->nama,
    //                 'nik' => $item->nik,
    //                 'tgl_lahir' => $item->tgl_lahir,
    //                 'umur_pasien' => $item->umur_pasien,
    //                 'nama_pj' => $item->nama_pj,
    //                 'status_pj' => $item->status_pj,
    //                 'no_hp' => $item->no_hp,
    //                 'pekerjaan' => $item->pekerjaan,
    //                 'alamat' => $item->alamat,
    //                 'golongan_darah' => $item->golongan_darah,
    //                 'tinggi_badan' => $item->tinggi_badan,
    //                 'berat_badan' => $item->berat_badan,
    //                 'antrian' => [
    //                     'no_antrian' => $item->no_antrian,
    //                     'tindakan' => $item->tindakan,
    //                     'status' => $item->status,
    //                     'soft_delete' => $item->soft_delete,
    //                     'created_at' => $item->created_at
    //                 ],
    //                 'rekam_bidan' => []
    //             ];
    //         }

    //         $riwayatItem = [];
    //         $riwayatItem['tanggal'] = $item->tanggal;

    //         $data[$item->no_reg]['rekam_bidan'][] = $riwayatItem;
    //     }

    //     $data = array_values($data);

    //     return response()->json([
    //         'message' => 'Sukses ambil data.',
    //         'data' => $data
    //     ], 200);
    // }

    // public function ListPasien(Request $request)
    // {
    //     $status = $request->query('status');
    //     $date = $request->query('date');

    //     $query = Pasien::with(['antrian', 'history'])
    //         ->orderBy('antrian.created_at', 'desc')
    //         ->first();

    //     if ($status !== null) {
    //         $query->where('antrian.status', $status);
    //     }

    //     if ($date !== null) {
    //         $query->whereDate('antrian.created_at', '=', $date);
    //     }

    //     $pasien = $query->get();

    //     return response()->json(
    //         [
    //             'message' => 'Sukses ambil data.',
    //             'data' => [$pasien],
    //         ],
    //         200
    //     );

    //     // $pasien = Pasien::with(['antrian', 'history'])
    //     //     // ->where('nik', $nik)
    //     //     ->orderBy('created_at', 'desc')
    //     //     ->first();

    //     // if ($pasien) {
    //     //     return response()->json([
    //     //         'message' => 'Sukses ambil data.',
    //     //         'data' => [$pasien]
    //     //     ], 200);
    //     // }
    //     // $status = $request->query('status');
    //     // $date = $request->query('date');

    //     // $query = Pasien::with(['antrian, history'])->orderBy('created_at', 'desc');

    //     // if ($status !== null) {
    //     //     $query->where('status', $status);
    //     // }

    //     // if ($date !== null) {
    //     //     $query->whereDate('created_at', '=', $date);
    //     // }

    //     // $pasien = $query->get();

    //     // if ($pasien) {
    //     //     return response()->json(
    //     //         [
    //     //             'message' => 'Sukses ambil data.',
    //     //             'data' => $pasien
    //     //         ],
    //     //         200
    //     //     );
    //     // }
    // }
}
