<?php

namespace App\Http\Controllers;

use App\Models\Diagnosa;
use App\Models\Obat;
use App\Models\Pasien;
use App\Models\Therapy;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ReferenceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
#==================TAMBAHAN WILDAN ====================
    public function refRekKehamilan() 
    {
        $rekamKehamilan = CatKehamilanModel::all();
        if ($rekamKehamilan) {
            return response()->json(
                [
                    'message' => 'sukses mengambil data.',
                    'data' => $rekamKehamilan
                ],
                200
            );
        }
    }

    public function refRegKehamilan(Request $request)
    {
        $noreg = $request->input('no_reg');
        $data = RegKehamilanModel::where('no_reg', $noreg)->get();
        if($data->isEmpty()) {
            return response()->json(
                [
                    'message' => "tidak ada data yang dapat diambil"
                ]
                );
        }
        return response()->json([
            'message' => 'data ditemukan',
            'data' => $data
        ],200);
    }
# =================================================
    public function refDiagnosa()
    {
        $diagnosa = Diagnosa::all();

        if ($diagnosa) {
            return response()->json(
                [
                    'message' => 'Sukses! Ambil Data.',
                    'data' => $diagnosa
                ],
                200
            );
        }
    }

    public function refTherapy()
    {
        $therapy = Therapy::all();

        if ($therapy) {
            return response()->json(
                [
                    'message' => 'Sukses! Ambil Data.',
                    'data' => $therapy
                ],
                200
            );
        }
    }

    public function refObat()
    {
        $obat = Obat::all();

        if ($obat) {
            return response()->json(
                [
                    'message' => 'Sukses! Ambil Data.',
                    'data' => $obat
                ],
                200
            );
        }
    }
    //
}
