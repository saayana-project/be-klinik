<?php

namespace App\Http\Controllers;

use App\Models\Antrian;
use App\Models\History;
use App\Models\Imunisasi;
use App\Models\Pasien;
use App\Models\RegisKehamilan;
use App\Models\RekamKehamilan;
use App\Models\RekamKMS;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;


use Laravel\Lumen\Routing\Controller as BaseController;

class BidanController extends BaseController
{
    public function CountAntrian(Request $request)
    {
        $today = Carbon::today();

        $count = Antrian::whereDate('created_at', $today)
            ->where('status', '2')
            ->count();

        return response()->json([
            'message' => 'Sukses ambil data.',
            'count' => $count
        ]);
    }

    public function ListPasien(Request $request)
    {
        $tindakan = $request->query('tindakan');
        $date = $request->query('date');

        $query = Antrian::join('pasien', 'antrian.no_reg', '=', 'pasien.no_reg')
            ->join('rekam_bidan', 'pasien.no_reg', '=', 'rekam_bidan.no_reg')
            ->select('pasien.*', 'antrian.no_antrian', 'antrian.created_at', 'antrian.tindakan', 'antrian.status', 'antrian.soft_delete', 'rekam_bidan.tanggal')
            ->where('antrian.status', 2)
            ->orderBy('antrian.created_at', 'desc');

        if ($tindakan !== null) {
            $query->where('antrian.tindakan', $tindakan);
        }

        if ($date !== null) {
            $query->whereDate('antrian.created_at', '=', $date);
        }

        $rawData = $query->get();

        $data = [];
        foreach ($rawData as $item) {
            if (!isset($data[$item->no_reg])) {
                $data[$item->no_reg] = [
                    'no_reg' => $item->no_reg,
                    'nama' => $item->nama,
                    'nik' => $item->nik,
                    'tgl_lahir' => $item->tgl_lahir,
                    'umur_pasien' => $item->umur_pasien,
                    'nama_pj' => $item->nama_pj,
                    'status_pj' => $item->status_pj,
                    'no_hp' => $item->no_hp,
                    'pekerjaan' => $item->pekerjaan,
                    'alamat' => $item->alamat,
                    'golongan_darah' => $item->golongan_darah,
                    'tinggi_badan' => $item->tinggi_badan,
                    'berat_badan' => $item->berat_badan,
                    'antrian' => [
                        'no_antrian' => $item->no_antrian,
                        'tindakan' => $item->tindakan,
                        'status' => $item->status,
                        'soft_delete' => $item->soft_delete,
                        'created_at' => $item->created_at
                    ],
                    'rekam_bidan' => []
                ];
            }

            $riwayatItem = [];
            $riwayatItem['tanggal'] = $item->tanggal;

            $data[$item->no_reg]['rekam_bidan'][] = $riwayatItem;
        }

        $data = array_values($data);

        return response()->json([
            'message' => 'Sukses ambil data.',
            'data' => $data
        ], 200);
    }

    public function DetailAntrianImunisasi(Request $request)
    {
        $no_reg = $request->query('no_reg');

        $query = Antrian::join('pasien', 'antrian.no_reg', '=', 'pasien.no_reg')
            ->select('pasien.*', 'antrian.no_antrian', 'antrian.created_at', 'antrian.tindakan')
            ->orderBy('antrian.created_at', 'desc');

        if ($no_reg !== null) {
            $query->where('pasien.no_reg', $no_reg);
        }

        $rawData = $query->get();

        $data = [];
        foreach ($rawData as $item) {
            if (!isset($data[$item->no_reg])) {
                $data[$item->no_reg] = [
                    'no_reg' => $item->no_reg,
                    'nama' => $item->nama,
                    'tgl_lahir' => $item->tgl_lahir,
                    'umur_pasien' => $item->umur_pasien,
                    'berat_badan' => $item->berat_badan,
                    'antrian' => [
                        'no_antrian' => $item->no_antrian,
                        'tindakan' => $item->tindakan,
                        'created_at' => $item->created_at
                    ],
                ];
            }
        }

        $data = array_values($data);

        return response()->json([
            'message' => 'Sukses ambil data.',
            'data' => $data
        ], 200);
    }

    public function ListKMS(Request $request)
    {
        $no_reg = $request->query('no_reg');

        $query = RekamKMS::query();

        if ($no_reg !== null) {
            $query->where('no_reg', $no_reg);
        }

        $query->orderBy('created_at', 'asc');

        $rawData = $query->get();

        $data = [];
        foreach ($rawData as $item) {
            $data[$item->no_reg]['no_reg'] = $item->no_reg;
            $data[$item->no_reg]['rekam_kms'][] = [
                'umur_pasien' => $item->umur_pasien,
                'bulan_penimbangan' => $item->bulan_penimbangan,
                'berat_badan' => $item->berat_badan,
                'nt' => $item->nt,
                'asi_ekslusif' => $item->asi_ekslusif,
            ];
        }

        $data = array_values($data);

        return response()->json([
            'message' => 'Sukses ambil data.',
            'data' => $data
        ], 200);
    }

    public function CreateKMS(Request $request)
    {
        $this->validate($request, [
            'no_reg' => 'required',
            'umur_pasien' => 'required',
            'bulan_penimbangan' => 'required',
            'berat_badan' => 'required',
            'nt' => 'required',
            'asi_ekslusif' => 'required',
        ]);

        $pasien = Pasien::firstOrNew(['no_reg' => $request->no_reg]);
        $pasien->umur_pasien = $request->umur_pasien;
        $pasien->berat_badan = $request->berat_badan;

        $rekam_kms = new RekamKMS;
        $rekam_kms->no_reg = $request->no_reg;
        $rekam_kms->umur_pasien = $request->umur_pasien;
        $rekam_kms->bulan_penimbangan = $request->bulan_penimbangan;
        $rekam_kms->berat_badan = $request->berat_badan;
        $rekam_kms->nt = $request->nt;
        $rekam_kms->asi_ekslusif = $request->asi_ekslusif;

        if ($pasien->no_reg == null) {
            return response()->json(['message' => 'No Registrasi tidak boleh kosong!'], 401);
        } else {
            try {
                $pasien->save();
                $rekam_kms->save();
            } catch (QueryException $exception) {
                return response()->json(['message' => 'Terjadi kesalahan saat menyimpan data!'], 401);
            }
            $pasien->save();
            $rekam_kms->save();
            return response()->json(['message' => 'Berhasil menambah pasien!'], 200);
        }
    }

    public function PenangananSelesai(Request $request, $no_antrian)
    {
        try {
            $antrian = Antrian::where(['no_antrian' => $no_antrian])->firstOrFail();
            $antrian->soft_delete = 99;
            $antrian->save();

            return response()->json(['message' => 'Data antrian berhasil diupdate'], 200);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['message' => 'Data antrian tidak ditemukan'], 404);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Terjadi kesalahan pada server'], 500);
        }
    }

    public function ListImunisasi(Request $request)
    {
        $no_reg = $request->query('no_reg');

        $query = Imunisasi::query();

        if ($no_reg !== null) {
            $query->where('no_reg', $no_reg);
        }

        // Menggunakan first() untuk mengambil single object
        $data = $query->first();

        if ($data) {
            return response()->json([
                'message' => 'Sukses ambil data.',
                'data' => $data
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data tidak ditemukan.',
                'data' => null
            ], 404);
        }
    }

    public function UpdateImunisasi(Request $request)
    {
        try {
            $this->validate($request, [
                'no_reg' => 'required',
            ]);

            $imunisasi = Imunisasi::where(['no_reg' => $request->no_reg])->firstOrFail();
            if ($request->hepatitis_b != null) {
                $imunisasi->hepatitis_b = $request->hepatitis_b;
            }
            if ($request->bcg != null) {
                $imunisasi->bcg = $request->bcg;
            }
            if ($request->polio_tetes_1 != null) {
                $imunisasi->polio_tetes_1 = $request->polio_tetes_1;
            }
            if ($request->dpt_hb_hib_1 != null) {
                $imunisasi->dpt_hb_hib_1 = $request->dpt_hb_hib_1;
            }
            if ($request->polio_tetes_2 != null) {
                $imunisasi->polio_tetes_2 = $request->polio_tetes_2;
            }
            if ($request->pcv_1 != null) {
                $imunisasi->pcv_1 = $request->pcv_1;
            }
            if ($request->dpt_hb_hib_2 != null) {
                $imunisasi->dpt_hb_hib_2 = $request->dpt_hb_hib_2;
            }
            if ($request->polio_tetes_3 != null) {
                $imunisasi->polio_tetes_3 = $request->polio_tetes_3;
            }
            if ($request->pcv_2 != null) {
                $imunisasi->pcv_2 = $request->pcv_2;
            }
            if ($request->dpt_hb_hib_3 != null) {
                $imunisasi->dpt_hb_hib_3 = $request->dpt_hb_hib_3;
            }
            if ($request->polio_tetes_4 != null) {
                $imunisasi->polio_tetes_4 = $request->polio_tetes_4;
            }
            if ($request->polio_suntik_ipv != null) {
                $imunisasi->polio_suntik_ipv = $request->polio_suntik_ipv;
            }
            if ($request->campak_rubela != null) {
                $imunisasi->campak_rubela = $request->campak_rubela;
            }
            if ($request->je != null) {
                $imunisasi->je = $request->je;
            }
            if ($request->pcv_3 != null) {
                $imunisasi->pcv_3 = $request->pcv_3;
            }
            $imunisasi->save();

            return response()->json(['message' => 'Data antrian berhasil diupdate'], 200);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['message' => 'Data antrian tidak ditemukan'], 404);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Terjadi kesalahan pada server'], 500);
        }
    }

    public function CreateKehamilan(Request $request)
    {
        $this->validate($request, [
            'no_reg' => 'required',
            'nama_suami' => 'required',
        ]);

        $regis_kehamilan = new RegisKehamilan;
        $regis_kehamilan->no_reg = $request->no_reg;
        $regis_kehamilan->nama_suami = $request->nama_suami;
        $regis_kehamilan->hpht = $request->hpht;
        $regis_kehamilan->tgl_htp = $request->tgl_htp;
        $regis_kehamilan->lingkar_lengan_atas = $request->lingkar_lengan_atas;
        $regis_kehamilan->berat_badan = $request->berat_badan;
        $regis_kehamilan->tinggi_badan = $request->tinggi_badan;
        $regis_kehamilan->kontrasepsi_sebelum_hamil = $request->kontrasepsi_sebelum_hamil;
        $regis_kehamilan->riwayat_penyakit = $request->riwayat_penyakit;
        $regis_kehamilan->riwayat_alergi = $request->riwayat_alergi;
        $regis_kehamilan->hamil_ke = $request->hamil_ke;
        $regis_kehamilan->jml_kehamilan = $request->jml_kehamilan;
        $regis_kehamilan->jml_keguguran = $request->jml_keguguran;
        $regis_kehamilan->jml_anak_hdp = $request->jml_anak_hdp;
        $regis_kehamilan->jml_lahir_mati = $request->jml_lahir_mati;
        $regis_kehamilan->jml_anak_lahir_kurang_bulan = $request->jml_anak_lahir_kurang_bulan;
        $regis_kehamilan->jarak_kehamilan = $request->jarak_kehamilan;
        $regis_kehamilan->stat_imunisasi_tt = $request->stat_imunisasi_tt;
        $regis_kehamilan->imunisasi_tt_terakhir = $request->imunisasi_tt_terakhir;
        $regis_kehamilan->penolong_persalinan_terakhir = $request->penolong_persalinan_terakhir;
        $regis_kehamilan->cara_persalinan_terakhir = $request->cara_persalinan_terakhir;
        $regis_kehamilan->tindakan_persalinan_terakhir = $request->tindakan_persalinan_terakhir;

        $rekam_kehamilan = new RekamKehamilan;
        $rekam_kehamilan->no_reg = $request->no_reg;

        if ($regis_kehamilan->no_reg == null) {
            return response()->json(['message' => 'No Registrasi tidak boleh kosong!'], 401);
        } else {
            try {
                $rekam_kehamilan->save();
                $regis_kehamilan->save();
            } catch (QueryException $exception) {
                return response()->json(['message' => 'Terjadi kesalahan saat menyimpan data!'], 401);
            }
            $rekam_kehamilan->save();
            $regis_kehamilan->save();
            return response()->json(['message' => 'Berhasil menambah pasien!'], 200);
        }
    }

    public function DetailKehamilan(Request $request)
    {
        $no_reg = $request->query('no_reg');

        $query = RegisKehamilan::query();

        if ($no_reg !== null) {
            $query->where('no_reg', $no_reg);
        }

        $query->orderBy('created_at', 'asc');

        $rawData = $query->get();

        $data = [];
        foreach ($rawData as $item) {
            $data[$item->no_reg]['no_reg'] = $item->no_reg;
            $data[$item->no_reg]['kehamilan'][] = [
                'nama_suami' => $item->nama_suami,
                'hpht' => $item->hpht,
                'tgl_htp' => $item->tgl_htp,
                'lingkar_lengan_atas' => $item->lingkar_lengan_atas,
                'berat_badan' => $item->berat_badan,
                'tinggi_badan' => $item->tinggi_badan,
                'kontrasepsi_sebelum_hamil' => $item->kontrasepsi_sebelum_hamil,
                'riwayat_penyakit' => $item->riwayat_penyakit,
                'riwayat_alergi' => $item->riwayat_alergi,
                'hamil_ke' => $item->hamil_ke,
                'jml_kehamilan' => $item->jml_kehamilan,
                'jml_keguguran' => $item->jml_keguguran,
                'jml_anak_hdp' => $item->jml_anak_hdp,
                'jml_lahir_mati' => $item->jml_lahir_mati,
                'jml_anak_lahir_kurang_bulan' => $item->jml_anak_lahir_kurang_bulan,
                'jarak_kehamilan' => $item->jarak_kehamilan,
                'stat_imunisasi_tt' => $item->stat_imunisasi_tt,
                'imunisasi_tt_terakhir' => $item->imunisasi_tt_terakhir,
                'penolong_persalinan_terakhir' => $item->penolong_persalinan_terakhir,
                'cara_persalinan_terakhir' => $item->cara_persalinan_terakhir,
                'tindakan_persalinan_terakhir' => $item->tindakan_persalinan_terakhir,
                'jml_lahir_mati' => $item->jml_lahir_mati,
            ];
        }

        $data = array_values($data);

        return response()->json([
            'message' => 'Sukses ambil data.',
            'data' => $data
        ], 200);
    }

    public function rekKehamilan(Request $request) {
        $currentDateTime = Carbon::now();
        $dateTime = $currentDateTime->format('Y-m-d');

        $this->validate($request, [
            'no_kehamilan' => 'required',
            'tanggal' => 'nullable|date',
            'keluhan_skrg' => 'required|string',
            'tekanan_drh' => 'nullable',
            'bb' => 'nullable',
            'tb' => 'nullable',
            'umur_khmln' => 'nullable',
            'tggi_fundus' => 'nullable',
            'letak_janin_Kep_Su_Li' => 'nullable|string',
            'dtk_jntg_janin' => 'nullable',
            'lab' => 'nullable|string',
            'pemeriksaan_khusus' => 'nullable|string',
            'tindakan' => 'nullable|string',
            'nasihat' => 'nullable|string',
            'keterangan' => 'nullable|string',
        ]);
        $no_kehamilan = $request->input('no_kehamilan');
        $tanggal = $request->input('tanggal');
        $keluhan_skrg = $request->input('keluhan_skrg');
        $tekanan_drh = $request->input('tekanan_drh');
        $bb = $request->input('bb');
        $tb = $request->input('tb');
        $umur_khmln = $request->input('umur_khmln');
        $tggi_fundus = $request->input('tggi_fundus');
        $letak_janin_Kep_Su_Li = $request->input('letak_janin_Kep_Su_Li');
        $dtk_jntg_janin = $request->input('dtk_jntg_janin');
        $lab = $request->input('lab');
        $pemeriksaan_khusus = $request->input('pemeriksaan_khusus');
        $tindakan = $request->input('tindakan');
        $nasihat = $request->input('nasihat');
        $keterangan = $request->input('keterangan');

        DB::table('rekam_kehamilan')->insert([
            'no_kehamilan' => $no_kehamilan,
            'tanggal' => $tanggal,
            'keluhan_skrg' => $keluhan_skrg,
            'tekanan_drh' => $tekanan_drh,
            'bb' => $bb,
            'tb' => $tb,
            'umur_khmln' => $umur_khmln,
            'tggi_fundus' => $tggi_fundus,
            'letak_janin_Kep_Su_Li' => $letak_janin_Kep_Su_Li,
            'dtk_jntg_janin' => $dtk_jntg_janin,
            'lab' => $lab,
            'pemeriksaan_khusus' => $pemeriksaan_khusus,
            'tindakan' => $tindakan,
            'nasihat' => $nasihat,
            'keterangan' => $keterangan
        ]);
        return response()->json(
            [
                'message' => 'catatan' . $no_kehamilan . "ditambahkan!"
            ]
            );
    }
}
