<?php

namespace App\Http\Controllers;

use App\Models\Antrian;
use App\Models\History;
use App\Models\Imunisasi;
use App\Models\Pasien;
use App\Models\RekamBidan;
use App\Models\RekamMedis;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller as BaseController;

class AdministrasiController extends BaseController
{
    public function CountAntrian(Request $request)
    {
        $today = Carbon::today();

        $count_pasien = Antrian::whereDate('created_at', $today)->count();
        $count_pasien_doktor = Antrian::whereDate('created_at', $today)->where('status', 1)->count();
        $count_pasien_bidan = Antrian::whereDate('created_at', $today)->where('status', 2)->count();

        return response()->json([
            'message' => 'Sukses ambil data.',
            'count_pasien' => $count_pasien,
            'count_pasien_doktor' => $count_pasien_doktor,
            'count_pasien_bidan' => $count_pasien_bidan
        ]);
    }

    public function GenerateNoReg(Request $request)
    {
        $pasien = Pasien::select('no_reg')
            ->orderBy('created_at', 'desc')
            ->first();

        if ($pasien) {
            return response()->json([
                'message' => 'Sukses ambil data.',
                'data' => $pasien
            ], 200);
        } else {
            return response()->json([
                'message' => 'Sukses ambil data.',
                'data' => ["no_reg" => null]
            ], 200);
        }
    }

    public function GenerateNoAntrian(Request $request)
    {
        // $today = date('Y-m-d')->toDateString();
        $status = $request->query('status');

        $query = Antrian::select('no_antrian')
            ->whereDate('created_at', '=', date('Y-m-d'))
            ->orderBy('created_at', 'desc');

        if ($status !== null) {
            $query->where('antrian.status', $status);
        }

        $antrian = $query->first();

        if ($antrian) {
            return response()->json([
                'message' => 'Sukses ambil data.',
                'data' => $antrian
            ], 200);
        } else {
            return response()->json([
                'message' => 'Sukses ambil data.',
                'data' => ["no_antrian" => null]
            ], 200);
        }
    }

    public function ListAntrian(Request $request)
    {
        $status = $request->query('status');
        $date = $request->query('date');

        $query = Antrian::join('pasien', 'antrian.no_reg', '=', 'pasien.no_reg')
            ->select('pasien.no_reg', 'antrian.no_antrian', 'pasien.nik', 'pasien.nama', 'pasien.tgl_lahir', 'pasien.umur_pasien', 'pasien.nama_pj', 'pasien.status_pj', 'pasien.no_hp', 'pasien.pekerjaan', 'pasien.alamat', 'pasien.keterangan', 'pasien.golongan_darah', 'pasien.tinggi_badan', 'pasien.berat_badan', 'antrian.status', 'antrian.soft_delete', 'antrian.created_at')
            ->orderBy('antrian.created_at', 'desc');

        if ($status !== null) {
            $query->where('antrian.status', $status);
        }

        if ($date !== null) {
            $query->whereDate('antrian.created_at', '=', $date);
        }

        $data = $query->get();

        return response()->json([
            'message' => 'Sukses ambil data.',
            'data' => $data
        ], 200);
    }

    public function CekNoReg(Request $request)
    {
        $no_reg = $request->input('no_reg');

        if (!$no_reg) {
            return response()->json([
                'message' => 'Nomor registrasi is required.',
            ], 400);
        }

        $pasien = Pasien::all('pasien.no_reg', 'pasien.nik', 'pasien.nama', 'pasien.nama_pj', 'pasien.status_pj', 'pasien.no_hp', 'pasien.pekerjaan', 'pasien.alamat', 'pasien.tinggi_badan', 'pasien.berat_badan')
            ->where('no_reg', $no_reg)
            ->first();

        if ($pasien) {
            return response()->json([
                'message' => 'Sukses ambil data.',
                'data' => $pasien
            ], 200);
        }

        return response()->json([
            'message' => 'Sukses ambil data.',
            'data' => null,
        ], 404);
    }

    public function CreatePasienDoktorBaru(Request $request)
    {
        $this->validate($request, [
            // PASIEN
            'no_reg' => 'required',
            'nama' => 'required',
            'tgl_lahir' => 'required',
            'umur_pasien' => 'required',
            'nama_pj' => 'required',
            'status_pj' => 'required',
            'no_hp' => 'required',
            'pekerjaan' => 'required',
            'alamat' => 'required',
            'golongan_darah' => 'required',
            'tinggi_badan' => 'required',
            'berat_badan' => 'required',

            // ANTRIAN
            'no_antrian' => 'required',
            'status' => 'required',

            // REKAM MEDIS
            'tanggal' => 'required',
        ]);

        $pasien = new Pasien;
        $pasien->no_reg = $request->no_reg;
        $pasien->nik = $request->nik;
        $pasien->nama = $request->nama;
        $pasien->tgl_lahir = $request->tgl_lahir;
        $pasien->umur_pasien = $request->umur_pasien;
        $pasien->nama_pj = $request->nama_pj;
        $pasien->status_pj = $request->status_pj;
        $pasien->no_hp = $request->no_hp;
        $pasien->pekerjaan = $request->pekerjaan;
        $pasien->alamat = $request->alamat;
        $pasien->keterangan = $request->keterangan;
        $pasien->golongan_darah = $request->golongan_darah;
        $pasien->tinggi_badan = $request->tinggi_badan;
        $pasien->berat_badan = $request->berat_badan;

        $antrian = new Antrian;
        $antrian->no_reg = $request->no_reg;
        $antrian->no_antrian = $request->no_antrian;
        $antrian->status = $request->status;
        $antrian->soft_delete = 0;

        $rekam_medis = new RekamMedis;
        $rekam_medis->no_reg = $request->no_reg;
        $rekam_medis->tanggal = $request->tanggal;

        if ($pasien->no_reg == null) {
            return response()->json(['message' => 'No Registrasi tidak boleh kosong!'], 401);
        } else {
            try {
                $pasien->save();
                $antrian->save();
                $rekam_medis->save();
            } catch (QueryException $exception) {
                // if ($exception->errorInfo[1] === 1062) {
                //     return response()->json(['message' => 'NIK pasien sudah terdaftar!'], 401);
                // }
                return response()->json(['message' => 'Terjadi kesalahan saat menyimpan data!'], 401);
            }
            $pasien->save();
            $antrian->save();
            $rekam_medis->save();
            return response()->json(['message' => 'Berhasil menambah pasien!'], 200);
        }
    }

    public function CreatePasienBidanBaru(Request $request)
    {
        $this->validate($request, [
            // PASIEN
            'no_reg' => 'required',
            'nama' => 'required',
            'tgl_lahir' => 'required',
            'umur_pasien' => 'required',
            'nama_pj' => 'required',
            'status_pj' => 'required',
            'no_hp' => 'required',
            'pekerjaan' => 'required',
            'alamat' => 'required',
            'golongan_darah' => 'required',
            'tinggi_badan' => 'required',
            'berat_badan' => 'required',

            // ANTRIAN
            'no_antrian' => 'required',
            'status' => 'required',
            'tindakan' => 'required',

            // REKAM BIDAN
            'tanggal' => 'required',
        ]);

        $pasien = new Pasien($request->only(['no_reg', 'nik', 'nama', 'tgl_lahir', 'umur_pasien', 'nama_pj', 'status_pj', 'no_hp', 'pekerjaan', 'alamat', 'keterangan', 'golongan_darah', 'tinggi_badan', 'berat_badan']));

        // Buat dan simpan data Antrian
        $antrian = new Antrian($request->only(['no_reg', 'no_antrian', 'status', 'tindakan']));
        $antrian->soft_delete = 0;

        // Buat dan simpan data RekamBidan
        $rekam_bidan = new RekamBidan($request->only(['no_reg', 'tanggal']));

        try {
            $pasien->save();
            $antrian->save();
            $rekam_bidan->save();

            // Cek tindakan dan simpan data Imunisasi jika tindakan == 3
            if ($request->tindakan == '2') {
                $imunisasi = new Imunisasi(['no_reg' => $request->no_reg]);
                // Setel atribut lain untuk imunisasi jika diperlukan
                $imunisasi->save();
            }

            return response()->json(['message' => 'Berhasil menambah pasien dan data terkait!'], 200);
        } catch (\Exception $exception) {
            // Tangani kesalahan saat menyimpan data
            return response()->json(['message' => 'Terjadi kesalahan saat menyimpan data: ' . $exception->getMessage()], 500);
        }
    }

    // public function CreatePasienExist(Request $request)
    // {
    //     $this->validate($request, [
    //         'no_antrian' => 'required',
    //         'nik' => 'required',
    //         'status' => 'required',
    //         'berat_badan' => 'required',
    //         'tinggi_badan' => 'required',
    //         'tensi_darah' => 'required',
    //     ]);

    //     $antrian = new Antrian;
    //     $antrian->nik = $request->nik;
    //     $antrian->no_antrian = $request->no_antrian;
    //     $antrian->status = $request->status;
    //     $antrian->soft_delete = 0;

    //     $history = new RekamMedis;
    //     $history->nik = $request->nik;
    //     $history->berat_badan = $request->berat_badan;
    //     $history->tinggi_badan = $request->tinggi_badan;
    //     $history->tensi_darah = $request->tensi_darah;

    //     if ($history->nik == null) {
    //         return response()->json(['message' => 'Nik tidak boleh kosong!'], 401);
    //     } else {
    //         // try {
    //         //     $antrian->save();
    //         //     $history->save();
    //         // } catch (QueryException $exception) {
    //         //     // if ($exception->errorInfo[1] === 1062) {
    //         //     //     return response()->json(['message' => 'NIK pasien sudah terdaftar!'], 401);
    //         //     // }
    //         //     return response()->json(['message' => 'Terjadi kesalahan saat menyimpan data!'], 401);
    //         // }
    //         $antrian->save();
    //         $history->save();
    //         return response()->json(['message' => 'Berhasil menambah pasien!'], 200);
    //     }
    // }
}
