<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = User::where('email', $request->input('email'))->first();

        if (!$user || !Hash::check($request->input('password'), $user->password)) {
            return response()->json(['message' => 'Email atau password salah.'], 401);
        }

        return response()->json(
            [
                'message' => 'Sukses! Login berhasil.',
                'data' => [
                    'email' => $user->email,
                    'nama_pegawai' => $user->pegawai ? $user->pegawai->nama_pegawai : null, // Replace 'name' with the actual column name for the pegawai's name
                    'roles' => $user->kode_jabatan,
                ]
            ],
            200
        );
    }
    //
}
