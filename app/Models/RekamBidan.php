<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Lumen\Auth\Authorizable;

class RekamBidan extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    protected $table = "rekam_bidan";
    protected $primaryKey = 'rekam_bidan_id';

    protected $fillable = [
        'no_reg',
        'tanggal',
        'pilihan_anamnesa_id',
        'pilihan_diagnosa_id',
        'pilihan_therapy_id',
        'tensi_darah',
        'soft_delete',
        'created_at',
        'updated_at',
    ];
}
