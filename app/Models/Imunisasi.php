<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Laravel\Lumen\Auth\Authorizable;

class Imunisasi extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    protected $table = "imunisasi";
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'no_reg',
        'hepatitis_b',
        'bcg',
        'polio_tetes_1',
        'dpt_hb_hib_1',
        'polio_tetes_2',
        'pcv_1',
        'dpt_hb_hib_2',
        'polio_tetes_3',
        'pcv_2',
        'dpt_hb_hib_3',
        'polio_tetes_4',
        'polio_suntik_ipv',
        'campak_rubela',
        'je',
        'pcv_3',
        'created_at',
        'updated_at'
    ];
}
