<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Laravel\Lumen\Auth\Authorizable;

class History extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    public $table = "history";

    protected $primaryKey = 'history_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'history_id', 'nik', 'berat_badan', 'tensi_darah', 'anamnesa', 'kode_diagnosa', 'kode_therapy'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var string[]
     */
    protected $hidden = [];

    public function pasien(): HasOne
    {
        return $this->hasOne(Pasien::class, 'nik', 'nik');
    }

    public function diagnosa(): BelongsTo
    {
        return $this->belongsTo(Diagnosa::class, 'kode_diagnosa', 'kode_diagnosa');
    }

    public function therapy(): BelongsTo
    {
        return $this->belongsTo(Therapy::class, 'kode_therapy', 'kode_therapy');
    }

    public function obat(): BelongsTo
    {
        return $this->belongsTo(Obat::class, 'kode_obat', 'kode_obat');
    }
}
