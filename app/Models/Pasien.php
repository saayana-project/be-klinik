<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Lumen\Auth\Authorizable;

class Pasien extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    protected $table = "pasien";
    protected $primaryKey = 'pasien_id';

    protected $fillable = [
        'no_reg',
        'nik',
        'nama',
        'tgl_lahir',
        'umur_pasien',
        'nama_pj',
        'status_pj',
        'no_hp',
        'pekerjaan',
        'alamat',
        'keterangan',
        'golongan_darah',
        'tinggi_badan',
        'berat_badan',
        'created_at',
        'updated_at',
    ];
}
