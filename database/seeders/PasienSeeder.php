<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class PasienSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('pasien')->insert([
            'no_reg' => '24022500001',
            'nik' => '3277021234567890',
            'nama' => 'Pasien 1',
            'tgl_lahir' => '1970-01-01',
            'umur_pasien' => 54,
            'nama_pj' => 'Mega',
            'status_pj' => 'Istri',
            'no_hp' => '085151409649',
            'pekerjaan' => 'Petugas KPPS',
            'alamat' => 'Jalan Pasteur',
            'keterangan' => null,
            'golongan_darah' => 'B',
            'tinggi_badan' => 174,
            'berat_badan' => 60,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
