<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $jabatan = [
            ['nama_jabatan' => 'Super Admin', 'kode_jabatan' => 99, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['nama_jabatan' => 'Admin', 'kode_jabatan' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['nama_jabatan' => 'Doktor', 'kode_jabatan' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['nama_jabatan' => 'Bidan', 'kode_jabatan' => 3, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['nama_jabatan' => 'Perawat', 'kode_jabatan' => 4, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['nama_jabatan' => 'Farmasi', 'kode_jabatan' => 5, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];

        DB::table('jabatan')->insert($jabatan);
    }
}
