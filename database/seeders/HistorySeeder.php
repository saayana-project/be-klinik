<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class HistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('history')->insert([
            'nik' => 320072,
            'berat_badan' => 60,
            'tinggi_badan' => 170,
            'tensi_darah' => '22/7',
            'anamnesa' => null,
            'kode_diagnosa' => null,
            'kode_therapy' => null,
            'kode_obat' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
