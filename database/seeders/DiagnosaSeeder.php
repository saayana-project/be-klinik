<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class DiagnosaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $diagnosa = [
            ['kode_diagnosa' => 1, 'nama_diagnosa' => 'Diagnosa 1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['kode_diagnosa' => 2, 'nama_diagnosa' => 'Diagnosa 2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['kode_diagnosa' => 3, 'nama_diagnosa' => 'Diagnosa 3', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['kode_diagnosa' => 4, 'nama_diagnosa' => 'Diagnosa 4', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['kode_diagnosa' => 5, 'nama_diagnosa' => 'Diagnosa 5', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];
        DB::table('diagnosa')->insert($diagnosa);
    }
}
