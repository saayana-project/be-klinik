<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    // public function run(): void
    // {
    //     DB::table('users')->insert([
    //         'email' => 'super_admin@example.com',
    //         'password' => Hash::make('superadmin'),
    //         'kode_jabatan' => 99,
    //         'created_at' => Carbon::now(),
    //         'updated_at' => Carbon::now()
    //     ]);
    // }

    public function run(): void
    {
        $users = [
            ['email' => 'super_admin@example.com', 'password' => Hash::make('superadmin'), 'kode_jabatan' => 99, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['email' => 'administrasi@example.com', 'password' => Hash::make('admin'), 'kode_jabatan' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['email' => 'doktor@example.com', 'password' => Hash::make('doktor'), 'kode_jabatan' => 2, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['email' => 'bidan@example.com', 'password' => Hash::make('bidan'), 'kode_jabatan' => 3, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];

        DB::table('users')->insert($users);
    }
}
