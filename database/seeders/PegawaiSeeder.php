<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */

    public function run(): void
    {
        $pegawai = [
            ['nama_pegawai' => 'Gregian Bayu', 'kode_jabatan' => 99, 'tgl_lahir' => '2000-01-01', 'email' => 'super_admin@example.com', 'alamat' => 'Jalan Sekian', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['nama_pegawai' => 'Riski Juliansyah', 'kode_jabatan' => 1, 'tgl_lahir' => '2001-01-01', 'email' => 'administrasi@example.com', 'alamat' => 'Jalan Sekian', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['nama_pegawai' => 'Saya Doktor', 'kode_jabatan' => 2, 'tgl_lahir' => '2001-01-01', 'email' => 'doktor@example.com', 'alamat' => 'Jalan Sekian', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['nama_pegawai' => 'Saya Bidan', 'kode_jabatan' => 3, 'tgl_lahir' => '2001-01-01', 'email' => 'bidan@example.com', 'alamat' => 'Jalan Sekian', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];

        DB::table('pegawai')->insert($pegawai);
    }
}
