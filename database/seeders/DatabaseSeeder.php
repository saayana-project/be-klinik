<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(JabatanSeeder::class);
        $this->call(PegawaiSeeder::class);
        $this->call(UsersSeeder::class);
        // $this->call(DiagnosaSeeder::class);
        // $this->call(TherapySeeder::class);
        // $this->call(ObatSeeder::class);
        // $this->call(PasienSeeder::class);
        // $this->call(AntrianSeeder::class);
        // $this->call(HistorySeeder::class);
    }
}
