<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class ObatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $obat = [
            ['kode_obat' => 'O1', 'nama_obat' => 'Obat 1', 'satuan_obat' => 'Sirup', 'harga_obat' => '10000', 'stock_obat' => 99, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['kode_obat' => 'O2', 'nama_obat' => 'Obat 2', 'satuan_obat' => 'Sirup', 'harga_obat' => '20000', 'stock_obat' => 99, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['kode_obat' => 'O3', 'nama_obat' => 'Obat 3', 'satuan_obat' => 'Tablet', 'harga_obat' => '3000', 'stock_obat' => 99, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['kode_obat' => 'O4', 'nama_obat' => 'Obat 4', 'satuan_obat' => 'Tablet', 'harga_obat' => '5000', 'stock_obat' => 99, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['kode_obat' => 'O5', 'nama_obat' => 'Obat 5', 'satuan_obat' => 'Sirup', 'harga_obat' => '15000', 'stock_obat' => 99, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];
        DB::table('obat')->insert($obat);
    }
}
