<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class TherapySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $therapy = [
            ['kode_therapy' => 1, 'nama_therapy' => 'Therapy 1', 'harga_therapy' => '10000', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['kode_therapy' => 2, 'nama_therapy' => 'Therapy 2', 'harga_therapy' => '20000', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['kode_therapy' => 3, 'nama_therapy' => 'Therapy 3', 'harga_therapy' => '15000', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['kode_therapy' => 4, 'nama_therapy' => 'Therapy 4', 'harga_therapy' => '18000', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['kode_therapy' => 5, 'nama_therapy' => 'Therapy 5', 'harga_therapy' => '22000', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];
        DB::table('therapy')->insert($therapy);
    }
}
