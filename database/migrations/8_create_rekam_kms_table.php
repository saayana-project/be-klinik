<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**D
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rekam_kms', function (Blueprint $table) {
            $table->bigIncrements('rekam_kms_id');
            $table->string('no_reg');
            $table->string('umur_pasien');
            $table->date('bulan_penimbangan');
            $table->string('berat_badan');
            $table->string('nt');
            $table->string('asi_ekslusif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rekam_kms');
    }
};
