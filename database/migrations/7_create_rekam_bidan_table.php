<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rekam_bidan', function (Blueprint $table) {
            $table->bigIncrements('rekam_bidan_id');
            $table->string('no_reg');
            $table->date('tanggal');
            $table->integer('pilihan_anamnesa_id')->nullable();
            $table->integer('pilihan_diagnosa_id')->nullable();
            $table->integer('pilihan_therapy_id')->nullable();
            $table->string('tensi_darah')->nullable();
            $table->integer('soft_delete')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rekam_bidan');
    }
};
