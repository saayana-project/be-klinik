<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('imunisasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_reg')->unique();
            $table->date('hepatitis_b')->nullable()->default(null);
            $table->date('bcg')->nullable()->default(null);
            $table->date('polio_tetes_1')->nullable()->default(null);
            $table->date('dpt_hb_hib_1')->nullable()->default(null);
            $table->date('polio_tetes_2')->nullable()->default(null);
            $table->date('pcv_1')->nullable()->default(null);
            $table->date('dpt_hb_hib_2')->nullable()->default(null);
            $table->date('polio_tetes_3')->nullable()->default(null);
            $table->date('pcv_2')->nullable()->default(null);
            $table->date('dpt_hb_hib_3')->nullable()->default(null);
            $table->date('polio_tetes_4')->nullable()->default(null);
            $table->date('polio_suntik_ipv')->nullable()->default(null);
            $table->date('campak_rubela')->nullable()->default(null);
            $table->date('je')->nullable()->default(null);
            $table->date('pcv_3')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('imunisasi');
    }
};
