<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pasien', function (Blueprint $table) {
            $table->bigIncrements('pasien_id');
            $table->string('no_reg')->unique();
            $table->string('nik')->unique()->nullable();
            $table->string('nama');
            $table->date('tgl_lahir');
            $table->string('umur_pasien');
            $table->string('nama_pj');
            $table->string('status_pj');
            $table->string('no_hp');
            $table->string('pekerjaan');
            $table->string('alamat');
            $table->string('keterangan')->nullable();
            $table->string('golongan_darah');
            $table->string('tinggi_badan');
            $table->string('berat_badan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pasien');
    }
};
