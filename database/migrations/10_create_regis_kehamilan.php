<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('regis_kehamilan', function (Blueprint $table) {
            $table->bigIncrements('no_kehamilan');
            $table->string('no_reg')->unique();
            $table->string('nama_suami')->nullable();
            $table->date('hpht')->nullable();
            $table->date('tgl_htp')->nullable();
            $table->string('lingkar_lengan_atas')->nullable();
            $table->string('berat_badan')->nullable();
            $table->string('tinggi_badan')->nullable();
            $table->string('kontrasepsi_sebelum_hamil')->nullable();
            $table->string('riwayat_penyakit')->nullable();
            $table->string('riwayat_alergi')->nullable();
            $table->string('hamil_ke')->nullable();
            $table->string('jml_kehamilan')->nullable();
            $table->string('jml_keguguran')->nullable();
            $table->string('jml_anak_hdp')->nullable();
            $table->string('jml_lahir_mati')->nullable();
            $table->string('jml_anak_lahir_kurang_bulan')->nullable();
            $table->string('jarak_kehamilan')->nullable();
            $table->string('stat_imunisasi_tt')->nullable();
            $table->string('imunisasi_tt_terakhir')->nullable();
            $table->string('penolong_persalinan_terakhir')->nullable();
            $table->string('cara_persalinan_terakhir')->nullable();
            $table->string('tindakan_persalinan_terakhir')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('regis_kehamilan');
    }
};
