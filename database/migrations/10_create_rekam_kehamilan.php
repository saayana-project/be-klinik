<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rekam_kehamilan', function (Blueprint $table) {
            $table->bigIncrements('rekam_kehamilan_id');
            $table->string('no_reg');
            $table->date('tanggal')->nullable();
            $table->string('keluhan_sekarang')->nullable();
            $table->string('tekanan_darah')->nullable();
            $table->string('berat_badan')->nullable();
            $table->string('tinggi_badan')->nullable();
            $table->string('umur_kehamilan')->nullable();
            $table->string('tinggi_fundus')->nullable();
            $table->string('letak_janin_Kep_Su_Li')->nullable();
            $table->string('detak_jantung_janin')->nullable();
            $table->string('lab')->nullable();
            $table->string('pemeriksaan_khusus')->nullable();
            $table->string('tindakan')->nullable();
            $table->string('nasihat')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rekam_kehamilan');
    }
};
